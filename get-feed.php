<?php
/**
 * The feeds class
 * Gets feeds links and shuffles so news is mixed up
 * regardless of date
 * @package WP RSS Feed Aggregator
 * @author Maxwell Mandela
 */
class Feed_Amalgamator
{
    public $urls = array();
    public $data = array();

    public function addFeeds( array $feeds )
    {
        $this->urls = array_merge( $this->urls, array_values($feeds) );
    }

    public function grabRss()
    {
        foreach ( $this->urls as $feed )
        {
            $data = @new SimpleXMLElement( $feed, 0, true );
            if ( !$data )
                throw new Exception( 'Could not load: ' . $feed );
            foreach ( $data->channel->item as $item )
            {
                $this->data[] = $item;
            }
        }
    }

    public function amalgamate()
    {
        shuffle( $this->data );
        $temp = array();
        foreach ( $this->data as $item )
        {
            if ( !in_array($item->link, $this->links($temp)) )
            {
                $temp[] = $item;
            }
        }
        $this->data = $temp;
        shuffle( $this->data );
    }

    private function links( array $items )
    {
        $links = array();
        foreach ( $items as $item )
        {
            $links[] = $item->link;
        }
        return $links;
    }
}