<?php
/**
 * Plugin Name: WP RSS Feed Aggregator
 * Plugin URI: https://bitbucket.org/maxwell_mandela/wp-rss-feed-aggregator
 * Description: Get all kenya showbizz news in one simple reader.
 * Version: 1.0.2
 * Author: Maxwell Mandela
 * Author URI: http://maxwellmandela.github.io/
 *
 * @package WP RSS Feed Aggregator
 * @author Maxwell Mandela
 *
 */

require_once(ABSPATH .'wp-config.php');

/**
 * Declare global variables
 * Set database version
 */
global $wpdb;
global $wp_rss_feed_db_version;
$wp_rss_feed_db_version = '1.0';

/**
 * Create urls table
 * table columns::
	* id
	* time
	* url_title(url title)
	* url_desc
	* url 
 */
function wp_rss_feed_create_url_table() {
	global $wpdb;
	global $wp_rss_feed_db_version;
	$wp_rss_feed_db_version = '1.0';

	$table_name = $wpdb->prefix . 'rss_links';
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		url_title tinytext NOT NULL,
		url_desc text NOT NULL,
		url varchar(55) DEFAULT '' NOT NULL,
		PRIMARY KEY (id),
		UNIQUE (url)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'wp_rss_feed_db_version', $wp_rss_feed_db_version );
}

/**
 * Adds new url into the table
 */
function wp_rss_feed_add_url() {
	global $wpdb;

	$url = $_POST['url'];
	$desc = $_POST['url_desc'];
	$url_title = $_POST['url_title'];
	
	$table_name = $wpdb->prefix . 'rss_links';
	$wpdb->insert( 
		$table_name, 
		array( 
			'time' => current_time( 'mysql' ), 
			'url_title' => $url_title,
			'url_desc' => $desc, 
			'url'  => $url
		)
	);
}

/** 
* Get urls from the  table
* Enable edit, delete
* Get results into json and pull with angular js,(ease editing)
*/
function wp_rss_feed_show_links(){
	global $wpdb;
	$sql = "SELECT * FROM wp_rss_links order by id desc";
	$results = $wpdb->get_results($sql) or die(mysql_error());

    foreach( $results as $result ) {
    	print "<div class='list-group-item'>
    		<span class='text text-info'>".$result->id."</span>, 
    		<span class='text text-primary'>".$result->url_title."</span>
    		<span class='edit_url'>
    			<input type='text' value='".$result->url."' class='form-control input-sm'>
    			<input type='text' value='".$result->url_title."' class='form-control input-sm'>
    		</span>
    		</div>";
    }
    //print json_encode($results);
}

/**
 * Plugin settings
 * Add url and url title and description
 */
function wp_rss_feed_add_settings_link( $links ) {
    $settings_link = '<a href="options-general.php?page=wp_rss_feed_settings">' . __( 'Settings' ) . '</a>';
    array_push( $links, $settings_link );
  	return $links;
}

$plugin = plugin_basename( __FILE__ );
add_filter( "plugin_action_links_$plugin", 'wp_rss_feed_add_settings_link' );
add_action( 'admin_menu', 'wp_rss_feed_menu' );

function wp_rss_feed_menu() {
	add_options_page( 'WP RSS Feed Aggregator', 'WP RSS Feed Aggregator', 'manage_options', 'wp_rss_feed_settings', 'wp_rss_feed_options' );
}

function wp_rss_feed_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	include 'settings_page.php';
	if(isset($_POST['url'])){
		wp_rss_feed_add_url();
	}
}


/**
 * Get urls from the table into simplepie
 * Pull feed from the url
 * Get data into arrays and implode
 * Insert into posts
 * Update post if title and url same
 */

function wp_rss_feed_get_feed(){
	include 'get-feed.php';
	//get links from the database
	global $wpdb;
	$sql = "SELECT * FROM wp_rss_links ORDER BY RAND()";
	$results = $wpdb->get_results($sql) or die(mysql_error());

	//simple pie get data for each link
	$urls = array();
	foreach( $results as $result ) {
		$urls[] = $result->url;
	}

	try
	{
	    $feeds = new Feed_Amalgamator;
	    $feeds->addFeeds( $urls );
	    $feeds->grabRss();
	    $feeds->amalgamate();
	}
	catch ( exception $e )
	{
	    die( $e->getMessage() );
	}
	foreach ( $feeds->data as $item ) :
        extract( (array) $item );
    	?>
    	<div class="pin">
            <a href="<?php echo $link ?>" target="_blank"><h3 class="text text-primary"><?php echo $title; ?></h3></a>
            <p><em><?php echo $pubDate ?></em></p>
            <p><?php echo $description; ?></p>
        </div>
        <?php
    endforeach;
}

register_activation_hook( __FILE__, 'wp_rss_feed_create_url_table' ); // array(class, function)
register_activation_hook( __FILE__, 'wp_rss_feed_get_feed' );
add_action('get_news_feed', 'wp_rss_feed_get_feed');