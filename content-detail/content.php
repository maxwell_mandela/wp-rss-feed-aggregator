<!DOCTYPE html>
<html>
<head>
	<title>Content</title>
</head>
<body ng-app="myApp"> <!--  -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
<div class="container" ng-controller="AppController"> <!--  -->
	<button class="btn btn-primary" ng-click="getI('<?php echo "http://www.nation.co.ke/news/politics/William-Ruto-Gay-Rights-Homosexuality/-/1064/2775872/-/4voa0lz/-/index.html"?>')">Get Content</button>
	<button class="btn btn-primary" ng-click="getI('<?php echo "http://www.nation.co.ke/news/africa/US-vows-support-for-Nigeria-after-attacks/-/1066/2776536/-/dlidboz/-/index.html"?>')">Get Content 2</button>
	<div id="the_post" class="col-sm-7">
		<span class="text text-primary" id="loading"></span>
		<div id="the_post_header"></div>
		<div id="the_post_title"></div>
		<div id="the_post_body"></div>
	</div>
	<script type="text/javascript">
		    var app = angular.module('myApp', []);
			app.controller('AppController', function($scope, $http) {
				$scope.getI = function(data_href){
					$("#loading").html("please wait...");
					var request = $http({
					    method: "post",
					    url: "getcontent.php",
					    data: {
					        link: data_href,
					    },
					    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					});
					request.success(function (data) {
						$("#loading").html('');
						$("#the_post_title").html(data.title+"<br><a href='"+data_href+"'>Read from Source</a>");
						$("#the_post_header").html(data.header);
						$("#the_post_body").html(data.body);
					});
				}
			});

		 /* });
		});*/
	</script>
</div>