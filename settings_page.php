<style type="text/css">
	@import url("https://maxcdn.bootstrapcdn.com/bootswatch/3.3.4/cosmo/bootstrap.min.css");
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<div class="col-md-6">
	<h2 class="text text-primary">Enter Url for news feed</h2>
	<form action="" method="post">
		<input type="text" name="url_title" placeholder="url title here" class="form-control input-sm"><br>
		<input type="text" name="url_desc" placeholder="link description here" class="form-control input-sm"><br>
		<input type="text" name="url" placeholder="url here" class="form-control input-sm"><br>
		<button type="submit" class="btn btn-primary btn-sm"> Save</button>
	</form>
</div>

<div class="col-md-6">
	<h2 class="text text-primary">Added Feeds here</h2>
	<button class="btn btn-primary btn-md" id='gotoedit'>
		Edit links <span class='text text-right'><i class='glyphicon glyphicon-pencil'></i></span>
	</button><br>
	<?php
		wp_rss_feed_show_links();
		wp_rss_feed_get_feed();
	?>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.edit_url').hide();
		$("#gotoedit").click(function(){
			$("span.edit_url").fadeIn("slow");
		});

	  $.get("http://localhost/wordpress/wp-content/plugins/wp-rss-feed-aggregator/feeds.php", function(data, status){
	        //alert("Data: " + data + "\nStatus: " + status);
	    });

	});
	</script>
</div>
